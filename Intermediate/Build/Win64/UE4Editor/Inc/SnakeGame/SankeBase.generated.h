// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class ASnakeElementBase;
class AActor;
#ifdef SNAKEGAME_SankeBase_generated_h
#error "SankeBase.generated.h already included, missing '#pragma once' in SankeBase.h"
#endif
#define SNAKEGAME_SankeBase_generated_h

#define Snake_Source_SnakeGame_SankeBase_h_23_SPARSE_DATA
#define Snake_Source_SnakeGame_SankeBase_h_23_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execSnakeElementOverlapped);


#define Snake_Source_SnakeGame_SankeBase_h_23_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execSnakeElementOverlapped);


#define Snake_Source_SnakeGame_SankeBase_h_23_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesASankeBase(); \
	friend struct Z_Construct_UClass_ASankeBase_Statics; \
public: \
	DECLARE_CLASS(ASankeBase, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/SnakeGame"), NO_API) \
	DECLARE_SERIALIZER(ASankeBase)


#define Snake_Source_SnakeGame_SankeBase_h_23_INCLASS \
private: \
	static void StaticRegisterNativesASankeBase(); \
	friend struct Z_Construct_UClass_ASankeBase_Statics; \
public: \
	DECLARE_CLASS(ASankeBase, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/SnakeGame"), NO_API) \
	DECLARE_SERIALIZER(ASankeBase)


#define Snake_Source_SnakeGame_SankeBase_h_23_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ASankeBase(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ASankeBase) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ASankeBase); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ASankeBase); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ASankeBase(ASankeBase&&); \
	NO_API ASankeBase(const ASankeBase&); \
public:


#define Snake_Source_SnakeGame_SankeBase_h_23_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ASankeBase(ASankeBase&&); \
	NO_API ASankeBase(const ASankeBase&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ASankeBase); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ASankeBase); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ASankeBase)


#define Snake_Source_SnakeGame_SankeBase_h_23_PRIVATE_PROPERTY_OFFSET
#define Snake_Source_SnakeGame_SankeBase_h_20_PROLOG
#define Snake_Source_SnakeGame_SankeBase_h_23_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Snake_Source_SnakeGame_SankeBase_h_23_PRIVATE_PROPERTY_OFFSET \
	Snake_Source_SnakeGame_SankeBase_h_23_SPARSE_DATA \
	Snake_Source_SnakeGame_SankeBase_h_23_RPC_WRAPPERS \
	Snake_Source_SnakeGame_SankeBase_h_23_INCLASS \
	Snake_Source_SnakeGame_SankeBase_h_23_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Snake_Source_SnakeGame_SankeBase_h_23_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Snake_Source_SnakeGame_SankeBase_h_23_PRIVATE_PROPERTY_OFFSET \
	Snake_Source_SnakeGame_SankeBase_h_23_SPARSE_DATA \
	Snake_Source_SnakeGame_SankeBase_h_23_RPC_WRAPPERS_NO_PURE_DECLS \
	Snake_Source_SnakeGame_SankeBase_h_23_INCLASS_NO_PURE_DECLS \
	Snake_Source_SnakeGame_SankeBase_h_23_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> SNAKEGAME_API UClass* StaticClass<class ASankeBase>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Snake_Source_SnakeGame_SankeBase_h


#define FOREACH_ENUM_EMOVEMENTDIRECTION(op) \
	op(EMovementDirection::UP) \
	op(EMovementDirection::DOWN) \
	op(EMovementDirection::LEFT) \
	op(EMovementDirection::RIGHT) 

enum class EMovementDirection;
template<> SNAKEGAME_API UEnum* StaticEnum<EMovementDirection>();

PRAGMA_ENABLE_DEPRECATION_WARNINGS
